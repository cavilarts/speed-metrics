const lighthouse = require('lighthouse');
const chromeLauncher = require('chrome-launcher');
const fs = require('fs');

const  lighthouseGen = async (url) => {
  const chrome = await chromeLauncher.launch({chromeFlags: ['--headless']});
  const options = {output: 'json', onlyCategories: ['performance'], port: chrome.port};
  const runnerResults = await lighthouse(url, options);
  const report = await JSON.parse(runnerResults.report);

  const fcp = await report.audits['first-contentful-paint'].numericValue;
  const meaningful = await report.audits['first-meaningful-paint'].numericValue;
  const si = report.audits['speed-index'].numericValue;
  const interactive = report.audits['interactive'].numericValue;

  await chrome.kill();

  return {
    contentful: fcp,
    meaningful: meaningful,
    speedIndex: si,
    interactive: interactive
  }
};

module.exports = lighthouseGen;
