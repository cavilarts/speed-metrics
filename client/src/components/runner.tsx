import React, { useState } from 'react';
import Highcharts from 'highcharts';
import HightchartsReact from 'highcharts-react-official';

import './runner.css';

const Runner:React.FC = () => {
  const [url, setUrl] = useState('');
  const [times, setTimes] = useState(2);
  const [loadingChart, setLoadingChart] = useState(false);
  const [chartData, setCharData] = useState([]);

  return (
    <section>
      {loadingChart ? renderLoading() : null}
      {!loadingChart && !chartData.length ? renderForm(setUrl, setTimes, setLoadingChart, setCharData, url, times) : null}
      {chartData.length ? renderCharts(chartData, url) : null}
    </section>
  );
};

const renderForm = (setUrl:any, setTimes:any, setLoadingChart:any, setCharData:any, url: string, times: number) => {
  return (
    <div className="reportform">
      <h1>Page Performance Report Generator</h1>
      <div className="report-creator">
        <div className="form-control">
          <label htmlFor="url">Url</label>
          <input
            type="text"
            placeholder="Ex: https://www.wikipedia.org/"
            id="url"
            name="url"
            onChange={(evt) => {
              setUrl(evt.target.value);
            }}
            value={url}
          />
        </div>
        <div className="form-control">
          <label htmlFor="times">Times</label>
          <input
            type="number"
            min="2"
            max="20"
            id="times"
            name="times"
            onChange={(evt) => {setTimes(parseInt(evt.target.value))}}
            value={times} />
          </div>
        <div className="form-control">
          <input type="button" value="Run" onClick={ async (e) => {
            setLoadingChart(true);
            const charData = await fetchData(url, times);
            setLoadingChart(false);
            setCharData(charData.data);
          }}/>
        </div>
      </div>
    </div>
  );
}

const fetchData = (url: string, times: number) => {
  return fetch('http://localhost:3001/reports', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      url: url,
      times: times
    })
  })
    .then((res) => res.json());

};

const renderCharts = (chartData: any[], url:string) => {
  const contentful = chartData.map(data => data.contentful);
  const meaningful = chartData.map(data => data.meaningful);
  const speedIndex = chartData.map(data => data.speedIndex);
  const interactive = chartData.map(data => data.interactive);

  return (
    <>
      <div>
        <h1>{`Here is your report for: ${url}`}</h1>
        <input
          type="button"
          className="icon-floppy-disk"
          onClick={(e) => {
            saveReport(chartData, url);
          }}
        />
      </div>

      {renderChart(contentful, 'Contentful Paint')}
      {renderChart(meaningful, 'Meaningful Content')}
      {renderChart(speedIndex, 'Speed Index')}
      {renderChart(interactive, 'Time to Interactive')}
    </>
  )
};

const renderChart =(data:any, type: string) => {
  const options = {
    title: {
      text: type
    },
    yAxis: [{ // Primary yAxis
      labels: {
          format: '{value} Seconds',
      },
      title: {
          text: 'Time',
      }
    }],
    series: [
      {
        type: 'column',
        data: data
      }
    ]
  }

  return (
    <HightchartsReact
      highcharts={Highcharts}
      options={options}
    />
  );
}

const renderLoading = () => {
  return (
    <div className="loading">
      <div className="loading-spinner icon-spinner"></div>
      <div className="loading-text">Loading your report...</div>
    </div>
  )
}

const saveReport = (data:any, url: string) => {
  const generated = new Date();

  return fetch('http://localhost:3001/reports', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      name: `${url} - ${generated}`,
      report: data
    })
  })
    .then((res) => res.json());
}

export default Runner;