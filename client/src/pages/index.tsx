import React from 'react';

import Runner from '../components/runner';

const IndexPage:React.FC = () => {
  return(
    <Runner />
  );
};

export default IndexPage;
