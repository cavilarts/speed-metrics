import React from 'react';

import IndexPage from './pages/index';

function App() {
  return (
    <section>
      <IndexPage />
    </section>
  );
}

export default App;
