const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const reportSchema = new Schema({
  name: String,
  contentfulPaint: Number,
  meaningfulPaint: Number,
  speedIndex: Number,
  timeToInteractive: Number,
  date: Date
});

module.exports = mongoose.model('Report', reportSchema);
