const reports = require('../../reports/lighthouse');
const Report = require('../model/report');

exports.report = async (req, res) => {
  const {url, times} = req.body;
  const reportList = [];

  for (let i = 0; i < times; i++) {
    reportList.push(await reports(url));
  }

  res.status(200).json({data: reportList});
};

exports.report_save = (req, res) => {
  const {name, data} = req.body;

  data.map(d => {
    const report = new Report({
      name: name,
      ...d,
      date: new Date
    });

    report.save();
  });

  Report.find({}, (err, reports) => {
    res.send(200).json({data: reports});
  });
};

exports.reports_all = (req, res) => {
  Report.find({}, (err, reports) => {
    res.send(200).json({data: reports});
  });
};