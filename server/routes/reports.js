const { Router } = require('express');

const reportController = require('../controllers/reportController');

const router = Router();

router.post('/', reportController.report);
router.post('/save', reportController.save);
router.get('/all', reportController.reports_all);
// Todo get report by id

module.exports = router;