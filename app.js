const express = require('express');
const { connect, connection } = require('mongoose');
const path = require('path');
const cors = require('cors');

// Routes Lib
const indexRouter = require('./server/routes/index');
const reportRouter = require('./server/routes/reports');

// Application
const app = express();
app.use(cors());

// DB Connection
connect('mongodb://localhost:27017/speed-report', { useNewUrlParser: true, useUnifiedTopology: true });
connection.once('open', () => console.log('Connected to database'));

// Port
const port = process.env.PORT || 3001;

// App settings
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));

// Routes
app.use('/', indexRouter);
app.use('/reports', reportRouter);

// App listen
app.listen(port, () => {
  console.log(`Listening at ${port}`);
});